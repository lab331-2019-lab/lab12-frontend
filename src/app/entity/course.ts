export default class Student {
    id: string;
    courseId: string;
    courseName: string;
    content: string;
    lecturer: string;
  }
  